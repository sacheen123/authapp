<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 10/10/2018
 * Time: 14:38
 */


	require_once "config.php";
	unset($_SESSION['access_token']);
	$gclient->revokeToken();
	session_destroy();
	header('Location: login.php');
	exit();
?>