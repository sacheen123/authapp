<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 09/10/2018
 * Time: 00:16
 */

	require_once "config.php";

	if (isset($_SESSION['access_token']))
        $gclient->setAccessToken($_SESSION['access_token']);
    else if (isset($_GET['code'])) {
        $token = $gclient->fetchAccessTokenWithAuthCode($_GET['code']);
        $_SESSION['access_token'] = $token;
    } else {
        header('Location: login.php');
        exit();
    }

	$oAuth = new Google_Service_Oauth2($gclient);
	$userData = $oAuth->userinfo_v2_me->get();

	$_SESSION['id'] = $userData['id'];
	$_SESSION['email'] = $userData['email'];
	$_SESSION['gender'] = $userData['gender'];
	$_SESSION['picture'] = $userData['picture'];
	$_SESSION['familyName'] = $userData['familyName'];
	$_SESSION['givenName'] = $userData['givenName'];

	header('Location: index.php');
	exit();
?>